import datetime

from django.contrib.auth import logout, login, authenticate
from django.http import HttpRequest, HttpResponse, HttpResponseForbidden
from django.shortcuts import render, redirect

from resources import strings
from webadt.models import TlgUser


def accept_listeners(request: HttpRequest):
    if request.user.is_authenticated and request.user.is_superuser:
        data = request.POST
        for viewer_id in data:
            if data[viewer_id] == 'on':
                try:
                    user = TlgUser.objects.get(id=viewer_id)
                    user.privileges += ['listener']
                    user.save()
                except:
                    pass
        return redirect('viewers_list')
    else:
        return HttpResponseForbidden('403 Forbidden')


def login_handler(request: HttpRequest):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user:
        login(request, user)
    else:
        return render(request, 'login.html', context={'error': strings['ru']['wrong_credentials_error']})
    return redirect('index')


def logout_handler(request: HttpRequest):
    logout(request)
    return redirect('index')


def index(request: HttpRequest):
    if not request.user.is_authenticated or not request.user.is_superuser:
        return render(request, 'login.html')
    users_list = list(TlgUser.objects.filter(raw_privileges__gt=0))
    params = {
        'users_list': users_list,
    }
    return render(request, 'users_list.html', params)


def viewers_list(request: HttpRequest):
    if not request.user.is_authenticated or not request.user.is_superuser:
        return render(request, 'login.html')
    viewers_list = list(TlgUser.objects.filter(raw_privileges=0))
    params = {
        'viewers_list': viewers_list,
    }
    return render(request, 'viewers_list.html', params)
