from django.apps import AppConfig


class WebadtConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'webadt'
