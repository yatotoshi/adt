"""adt URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.urls import path
from django.contrib import admin
from django.urls import include
import webadt.views as views


urlpatterns = [
    path('', views.index, name='index'),
    path('viewers_list', views.viewers_list, name='viewers_list'),
    path('accept_listeners', views.accept_listeners),
    path('login', views.login_handler, name='login'),
    path('logout', views.logout_handler),
]
