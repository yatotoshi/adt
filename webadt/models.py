import random
from functools import reduce
from datetime import datetime, timedelta

import pytz
from django.db import models
from telegram import Update

from resources import strings
import locale


class TlgUser(models.Model):
    chat_id = models.IntegerField(unique=True, blank=False)
    username = models.CharField(max_length=128, blank=True, null=True)
    name = models.CharField(max_length=128)
    raw_privileges = models.IntegerField(default=0)

    privilege_set = {
        'viewer': int('000', 2),
        'listener': int('001', 2),
        'organizer': int('010', 2),
        'admin': int('100', 2)
    }

    def __str__(self):
        return f'{self.id}: {self.chat_id} ({self.name})'

    def get_privileges(self) -> list[str]:
        priv = []
        for status in self.privilege_set:
            if self.raw_privileges & self.privilege_set[status] == self.privilege_set[status]:
                priv.append(status)
        return priv

    def set_privileges(self, privileges: list[str]) -> None:
        self.raw_privileges = 0
        for status in privileges:
            self.raw_privileges |= self.privilege_set[status]

    def remove_privileges(self) -> None:
        self.raw_privileges = 0

    privileges = property(get_privileges, set_privileges, remove_privileges)

    def __beautify__(self, req_locale):
        keys = {
            'name': self.name,
            'chat_id': self.chat_id,
            'permissions': ', '.join([strings['ru'][f'{permission}_permission'] for permission in self.privileges])
        }
        return strings[req_locale]['beautified_user_core'].format(**keys)


class Event(models.Model):
    short = models.CharField(max_length=10, blank=True)
    title = models.CharField(max_length=256, blank=False)
    description = models.TextField(blank=False)
    lectors = models.CharField(max_length=256, blank=True)
    picture = models.ImageField(blank=True, null=True)
    date = models.DateTimeField(blank=False)
    deadline = models.DateTimeField(blank=True, null=True)
    seats = models.IntegerField(blank=True, null=True)
    organizer = models.ForeignKey(TlgUser, on_delete=models.SET_NULL, null=True, related_name='published_events')
    listeners = models.ManyToManyField(TlgUser, related_name='subscriptions', through='Feedback')

    def save(self, *args, **kwargs):
        if not self.deadline:
            self.deadline = self.date
        if self.deadline > self.date:
            raise AttributeError('deadline cannot be after the date of event')
        if not self.short or (self.short and self.short == ''):
            short = reduce(lambda prev_el, el: prev_el + el, random.choices(Event.get_base58(), k=5))
            tries = 0
            while Event.objects.filter(short=short).count() > 0 and tries <= 99:
                short = reduce(lambda prev_el, el: prev_el + el, random.choices(Event.get_base58(), k=5))
                tries += 1
            if tries == 99:
                raise RuntimeError('Length of event\'s short must be increased')
            else:
                self.short = short
        super(Event, self).save(*args, **kwargs)

    @staticmethod
    def get_base58():
        return '123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ'

    def __str__(self):
        return f'{self.title}: {self.organizer}'

    def __beautify__(self, req_locale):
        locale.setlocale(locale.LC_TIME, 'ru_RU.UTF-8')

        if self.organizer:
            organizer_name = self.organizer.name
        else:
            organizer_name = strings[req_locale]['deleted']
        if self.date is str:
            self.date = datetime.datetime.strptime(self.date, '%Y-%m-%d %H:%M')
        if self.deadline is str:
            self.deadline = datetime.datetime.strptime(self.deadline, '%Y-%m-%d %H:%M')
        keys = {
            'title': self.title,
            'description': self.description,
            'date': self.date.strftime('%d %b %Y (%a)'),
            'time': self.date.strftime('%H:%M'),
            'organizer_name': organizer_name,
        }
        beautified = ''
        if self.short and self.short != '':
            beautified += f'[{self.short}] '
        beautified += strings[req_locale]['beautified_event_core'] + '\n'
        if self.deadline != self.date:
            keys['deadline'] = self.deadline.strftime('%d %b %Y')
            beautified += '\n' + strings[req_locale]['beautified_event_deadline_set']
        if self.lectors:
            keys['lectors'] = self.lectors
            beautified += '\n' + strings[req_locale]['beautified_event_lectors_set']
        else:
            beautified += '\n' + strings[req_locale]['beautified_event_lectors_not_set']
        listener_count = self.listeners.count()
        keys['listener_count'] = listener_count
        beautified += '\n' + strings[req_locale]['beautified_event_listeners']
        today = datetime.now(tz=pytz.utc) + timedelta(hours=3)
        if self.seats:
            keys['free_seats'] = int(self.seats) - listener_count
            beautified += '\n' + strings[req_locale]['beautified_event_limited_seats']
        if self.feedback_set.exists() and self.date < today:
            keys['visited_count'] = self.feedback_set.filter(visited=True).count()
            keys['heart_count'] = self.feedback_set.filter(stars__gte=1).count()
            beautified += '\n' + strings[req_locale]['beautified_event_visited']
        return beautified.format(**keys)


class Feedback(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    user = models.ForeignKey(TlgUser, on_delete=models.CASCADE)
    review = models.CharField(max_length=512, blank=True)
    stars = models.IntegerField(blank=True, null=True)
    visited = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        if not self.visited:
            self.stars = 0
        super().save(*args, **kwargs)
