from telegram import InlineKeyboardButton

strings = {
    'ru': {
        'greeting':
            'Вас приветствует бот университетских анонсов АГУ.\n'
            'Чтобы узнать свой статус, проверьте клавиатуру.\n'
            'После сбоев в работе бота, выполните команду /start, чтобы сбросить состояние.',
        'event':
            'Событие',
        'success':
            'Выполнено!',
        'broadcast':
            'Рассылка',
        'signup_try':
            'Вы можете отменить регистрацию в любой момент, нажав на кнопку "/cancel".',
        'signup_name_ask':
            'Пожалуйста, введите свое имя и фамилию через пробел.',
        'signup_success':
            'Вы успешно зарегистрировались в системе, ожидайте одобрения администратора.',
        'signup_user_exists':
            'Вы уже зарегистрированы в системе.',
        'signup_resume':
            'Ваш идентификатор: {chat_id}, ваше имя: {name}.',
        'interrupt':
            'Хорошо, что-нибудь еще?',
        'current_events_button':
            'Актуальные анонсы',
        'signup_button':
            'Регистрация',
        'visited_events_button':
            'Архив посещенных',
        'subscriptions_events_button':
            'Мои записи',
        'create_event_button':
            'Создать мероприятие',
        'created_events_button':
            'Созданные мероприятия',
        'archive_events_button':
            'Архив проведенных',
        'event_broadcast_button':
            'Создать рассылку по мероприятию',
        'general_broadcast_button':
            'Создать всеобщую рассылку',
        'set_organizer_button':
            'Назначить организатора',
        'nothing_to_show':
            'Здесь пока что пусто :(',
        'current_events_message_title':
            'Актуальные мероприятия:',
        'subscriptions_events_message_title':
            'Вы собирались посетить:',
        'visited_events_message_title':
            'Вы посетили:',
        'created_events_message_title':
            'Вы организовали:',
        'archive_events_message_title':
            'Ваши архивные мероприятия:',
        'beautified_event_core':
            '{title}\n=───=\n{description}\n\nДата проведения: {date} в {time}',
        'beautified_event_deadline_set':
            'Конец записи: {deadline}',
        'beautified_event_lectors_set':
            'Читают: {lectors}\nОрганизатор: {organizer_name}',
        'beautified_event_lectors_not_set':
            'Организатор: {organizer_name}',
        'beautified_event_limited_seats':
            'Свободных мест: {free_seats}',
        'beautified_event_listeners':
            'Записались на мероприятие: {listener_count}',
        'beautified_event_visited':
            'Посетили мероприятие: {visited_count}\n'
            '- {heart_count} ♥ -',
        'subscribe_button':
            'Записаться',
        'unsubscribe_button':
            'Отписаться',
        'deleted':
            '<удален>',
        'no_privileges':
            'Извините, у вас нет нужных прав для этого действия.',
        'set_visited_button':
            'Посетил',
        'unset_visited_button':
            'Не посещал',
        'event_asking_title_message':
            'Введите заголовок мероприятия',
        'event_asking_description_message':
            'Введите описание',
        'event_asking_date_message':
            'Введите дату и время проведения мероприятия в формате YYYY-MM-DD HH:mm, например, 2021-04-19 15:00',
        'event_asking_deadline_message':
            'Введите последний день, доступный для записи на мероприятие (если записываться можно вплоть до '
            'начала мероприятия, пропустите этот этап) в формате YYYY-MM-DD, например, 2021-04-19',
        'event_asking_lectors_message':
            'Перечислите лекторов мероприятия (если вы сами проводите мероприятие, пропустите этот этап)',
        'event_asking_seats_message':
            'Введите количество мест (если места не ограничены, пропустите этот этап)',
        'event_asking_picture_message':
            'Отправьте картинку, которую хотите прикрепить к мероприятию',
        'event_asking_done_message':
            'Мероприятие уже публикуется!',
        'invalid_date_message':
            'Кажется, вы ввели некорректную дату.',
        'invalid_int_message':
            'Кажется, вы ввели некорректное число.',
        'error_occurred':
            'При записи мероприятия произошла ошибка.',
        'invalid_deadline_error':
            'Последний день записи не может быть после мероприятия или совпадать с днем проведения мероприятия',
        'broadcast_asking_text_message':
            'Отправьте текст рассылки',
        'broadcast_confirming_text_message':
            'Нажмите на /accept, чтобы подтвердить рассылку:',
        'event_state_unpassable_message':
            'Этот этап пропустить нелья.',
        'edit_event_button':
            'Редактировать',
        'delete_event_button':
            'Удалить',
        'confirmed_delete_event_button':
            'Подтвердить удаление',
        'feedback_event_button':
            'Оценить',
        'heart_event_button':
            '♡',
        'unheart_event_button':
            '♥',
        'broadcast_button':
            'Создать рассылку',
        'success_broadcast':
            'Рассылка завершена, ваше сообщение получило {user_count} пользователей',
        'event_not_found':
            'Событие с идентификатором {short} не найдено :(',
        'find_usage_error':
            'Вам нужно ввести идектификатор события после команды /find. Например: /find QiVF4.',
        'user_beautified':
            'Имя: {name};\nИдентификатор:{id};',
        'set_organizer_error':
            'Вам нужно ввести идентификатор пользователя после команды /set_organizer. Например /set_organizer 123456.',
        'unset_organizer_error':
            'Вам нужно ввести идентификатор пользователя после команды /unset_organizer. '
            'Например /set_organizer 123456.',
        'no_user_found_error':
            'Нет пользователя с таким идентификатором',
        'not_signup_error':
            'Вы не зарегистрированы в системе',
        'beautified_user_core':
            'Имя: {name};\nИдентификатор: {chat_id};\nПрава: {permissions}.',
        'too_long_name_error':
            'Имя не должно быть длиннее 255 символов',
        'viewer_permission':
            'гость',
        'listener_permission':
            'слушатель',
        'organizer_permission':
            'организатор',
        'admin_permission':
            'администратор',
        'wrong_credentials_error':
            'Неверное имя пользователя или пароль'
    }
}

# numerated (0, 1, 2, ...) states dictionary
_states_list = [
    # sign up states
    'NAME_ASKING',

    # create events states
    'E_TITLE_ASKING', 'E_DESCRIPTION_ASKING', 'E_DATE_ASKING', 'E_DEADLINE_ASKING',
    'E_LECTORS_ASKING', 'E_SEATS_ASKING', 'E_PICTURE_ASKING', 'E_DONE_ASKING',

    # leave feedback states
    'FB_STARS', 'FB_REVIEW',

    # broadcast states
    'B_TEXT_CONFIRMING', 'B_DONE_ASKING',
]
states = dict(zip(_states_list, list(map(lambda i: 's' + str(i), range(len(_states_list))))))
_indicators_list = [
    # paging
    'page_current_events', 'page_visited_events', 'page_subscribed_events', 'page_created_events',
    'page_created_archive_events', 'page_events',

    # actions
    'subscribe_toggle', 'set_event_visited', 'edit_event', 'delete_event', 'confirmed_delete_event',
    'feedback_event', 'heart_event',

    # other
    'choose_event', 'broadcast',
]
indicators = dict(zip(_indicators_list, list(map(lambda i: 'i' + str(i), range(len(_indicators_list))))))

key_lists = {
    'cancel': [['/cancel']],
    'pass': [['/pass']],
    'back': [['/back']],
    'accept': [['/accept']],
    'signup': [[strings['ru']['signup_button']]],
    'viewer': [[strings['ru']['current_events_button']]],
    'listener': [[strings['ru']['visited_events_button']], [strings['ru']['subscriptions_events_button']]],
    'organizer': [[strings['ru']['create_event_button']], [strings['ru']['created_events_button']],
                  [strings['ru']['archive_events_button']]],
    'admin': [[strings['ru']['general_broadcast_button']]]
}

move_buttons_ind = [
    indicators['page_events'],
    indicators['page_current_events'],
    indicators['page_visited_events'],
    indicators['page_subscribed_events'],
    indicators['page_created_events'],
    indicators['page_created_archive_events']
]

paginator_types = ('current', 'visited', 'subscriptions', 'created', 'archive')