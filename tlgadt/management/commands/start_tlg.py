from django.core.management.base import BaseCommand, CommandError
from tlgadt.main import run


class Command(BaseCommand):
    help = 'Runs telegram bot listening'

    def handle(self, *args, **options):
        run()
