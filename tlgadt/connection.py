import os
import logging

from telegram.ext import Updater

logger = logging.getLogger(__name__)


def connect(handler_list):
    updater = Updater(os.environ['tlg__bot_token'])
    updater.logger.setLevel(logging.ERROR)
    updater.bot.logger.setLevel(logging.ERROR)
    updater.dispatcher.logger.setLevel(logging.ERROR)
    logger.info('Starting connection...')
    for handler_def in handler_list:
        updater.dispatcher.add_handler(handler_def)

    if os.environ['tlg__connection_type'] == 'webhooks':
        if os.environ['tlg__connection_port'] == 'auto':
            port = int(os.environ.get('PORT', 5000))
        else:
            port = os.environ['tlg__connection_port']
        logger.info(f'Listening on port {port}')
        updater.start_webhook(listen="0.0.0.0",
                              port=port,
                              url_path=os.environ['tlg__bot_token'],
                              webhook_url=os.environ['tlg__connection_host'] + os.environ['tlg__bot_token'])
        logger.info('Webhooks listener is set')
    elif os.environ['tlg__connection_type'] == 'longpoll':
        updater.bot.delete_webhook()
        logger.info('Longpoll listener is set:')
        updater.start_polling()

    updater.idle()
