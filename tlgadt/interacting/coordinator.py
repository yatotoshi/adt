from telegram.ext import CommandHandler, MessageHandler, Filters, CallbackQueryHandler
from resources import *
import tlgadt.interacting.common_handlers as common_handlers

import tlgadt.interacting.user_handlers as user_handlers
import tlgadt.interacting.pagination_handlers as event_pagination_handlers
import tlgadt.interacting.broadcast_handlers as broadcast_handlers
import tlgadt.interacting.creating_event_handlers as creating_event_handlers
import tlgadt.interacting.action_handlers as action_handlers
import tlgadt.interacting.organizer_handlers as organizer_handlers

page_events_handlers = []

handler_list = [
    CommandHandler('start', common_handlers.start_handler),
    CommandHandler('find', event_pagination_handlers.find_handler),
    CommandHandler('change_name', user_handlers.change_name_handler),
    CommandHandler('account', user_handlers.account_handler),
    CommandHandler('set_organizer', organizer_handlers.set_organizer_handler),
    CommandHandler('unset_organizer', organizer_handlers.unset_organizer_handler),

    # CONVERSATIONS
    # signup
    user_handlers.signup_conversation,

    # broadcast
    broadcast_handlers.broadcast_conversation,

    # create event
    creating_event_handlers.create_event_conversation,

    # ACTION BUTTONS
    # subscribe event button
    CallbackQueryHandler(action_handlers.action_handler,
                         pattern=rf"^{indicators['subscribe_toggle']}::.*$"),

    # set event visited button
    CallbackQueryHandler(action_handlers.action_handler,
                         pattern=rf"^{indicators['set_event_visited']}::.*$"),

    # delete event button
    CallbackQueryHandler(action_handlers.action_handler,
                         pattern=rf"^{indicators['delete_event']}::.*$"),
    CallbackQueryHandler(action_handlers.action_handler,
                         pattern=rf"^{indicators['confirmed_delete_event']}::.*$"),

    # heart event button
    CallbackQueryHandler(action_handlers.action_handler,
                         pattern=rf"^{indicators['heart_event']}::.*$"),

    # PAGE EVENT HANDLERS
    CallbackQueryHandler(action_handlers.move_handler,
                         pattern=rf"^{indicators['page_events']}::.*$"),

    # current events
    MessageHandler(Filters.regex(f"^{strings['ru']['current_events_button']}$"),
                   event_pagination_handlers.page_events_handler),
    # subscribed events
    MessageHandler(Filters.regex(f"^{strings['ru']['subscriptions_events_button']}$"),
                   event_pagination_handlers.page_events_handler),
    # visited events
    MessageHandler(Filters.regex(f"^{strings['ru']['visited_events_button']}$"),
                   event_pagination_handlers.page_events_handler),

    # created events
    MessageHandler(Filters.regex(f"^{strings['ru']['created_events_button']}$"),
                   event_pagination_handlers.page_events_handler),

    # archive events
    MessageHandler(Filters.regex(f"^{strings['ru']['archive_events_button']}$"),
                   event_pagination_handlers.page_events_handler),
]