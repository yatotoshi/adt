import datetime
from typing import Iterable

from telegram import InlineKeyboardMarkup, Update
from telegram.ext import CallbackContext
from resources import *
from webadt.models import TlgUser, Event
from .tools import *
from .paginators import EventPaginator, FindEventPaginator
from .action_buttons import *
from .exceptions import *


@privileges_required(['listener'])
def page_events_handler(update: Update, context: CallbackContext) -> None:
    paginator_clicked = None
    for paginator_type in paginator_types:
        if update.message.text == strings['ru'][f'{paginator_type}_events_button']:
            paginator_clicked = paginator_type
            break

    if paginator_clicked is None:
        raise ValueError(f'Unknown paginator type: {paginator_clicked}')
    paginator = EventPaginator(paginator_clicked, 0, update, context)
    event, keyboard = paginator.get_page()

    if event:
        update.message.reply_text(
            text=beautify(event, paginator.get_title()),
            reply_markup=keyboard
        )
    else:
        update.message.reply_text(
            text=strings['ru']['nothing_to_show'],
            reply_markup=keyboard
        )


@privileges_required(['viewer'])
def find_handler(update: Update, context: CallbackContext) -> None:
    params = update.message.text.split()
    if len(params) != 2:
        response = strings['ru']['find_usage_error']
    else:
        short = params[1].replace('[', '').replace(']', '')
        paginator = FindEventPaginator(
            event_short=short,
            update=update,
            context=context
        )

        event, keyboard = paginator.get_page()

        if event:
            update.message.reply_text(
                text=beautify(event, paginator.get_title()),
                reply_markup=keyboard
            )
        else:
            update.message.reply_text(
                text=strings['ru']['event_not_found'].format(short=short),
                reply_markup=keyboard
            )
