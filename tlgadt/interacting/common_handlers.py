import logging

from resources import *
from tlgadt.interacting.tools import *

logger = logging.getLogger(__name__)


def start_handler(update: Update, _: CallbackContext) -> None:
    """Send a message when the command /start is issued."""
    logger.info(f'User {update.effective_user.id} evoked /start')
    update.message.reply_text(
        text=strings['ru']['greeting'],
        reply_markup=build_keyboard(update.effective_user.id)
    )
    return ConversationHandler.END


def cancel_handler(update: Update, _: CallbackContext) -> int:
    update.message.reply_text(
        text=strings['ru']['interrupt'],
        reply_markup=build_keyboard(update.effective_user.id)
    )
    return ConversationHandler.END


def pass_handler(update: Update, _: CallbackContext) -> int:
    return ConversationHandler.END

