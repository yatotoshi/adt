from telegram.ext import CommandHandler, MessageHandler, Filters, CallbackQueryHandler
from telegram import ReplyKeyboardMarkup

from webadt.models import Event
from . import common_handlers
from .action_buttons import ActionButton
from .tools import *


def creating_broadcast(update: Update, context: CallbackContext) -> str:
    if update.message:
        update.message.reply_text(
            strings['ru']['broadcast_asking_text_message'],
            reply_markup=ReplyKeyboardMarkup(key_lists['cancel'])
        )
        context.user_data['event_id'] = None
    else:
        update.callback_query.message.reply_text(
            strings['ru']['broadcast_asking_text_message'],
            reply_markup=ReplyKeyboardMarkup(key_lists['cancel'])
        )
        _, args = ActionButton.parse_arguments(update.callback_query.data)
        context.user_data['event_id'] = args['event_id']
    return states['B_TEXT_CONFIRMING']


def confirm_broadcast(update: Update, context: CallbackContext) -> str:
    user = TlgUser.objects.get(chat_id=update.effective_user.id)
    context.user_data['broadcast_text'] = update.message.text + f'\n{user.name}'
    if context.user_data['event_id']:
        event = Event.objects.get(id=context.user_data['event_id'])
        text = f'''{strings['ru']['broadcast_confirming_text_message']}
{strings['ru']['event']}: {event.title}
{strings['ru']['broadcast']}:\n{context.user_data['broadcast_text']}'''
    else:
        text = f'''{strings['ru']['broadcast_confirming_text_message']}
{strings['ru']['broadcast']}:\n{context.user_data['broadcast_text']}'''
    update.message.reply_text(
        text,
        reply_markup=ReplyKeyboardMarkup(key_lists['cancel'] + key_lists['accept'])
    )
    return states['B_DONE_ASKING']


@privileges_required(['organizer'])
def done_broadcast(update: Update, context: CallbackContext) -> None:
    user = TlgUser.objects.get(chat_id=update.effective_user.id)
    if context.user_data['event_id']:
        event = Event.objects.get(id=context.user_data['event_id'])
        users = event.listeners.all()
        if event.organizer == user or 'admin' in user.privileges:
            user_count = broadcast(users, context.user_data['broadcast_text'], context)
            update.message.reply_text(
                strings['ru']['success_broadcast'].format(user_count=user_count),
                reply_markup=build_keyboard(update.effective_user.id)
            )
            return ConversationHandler.END
        else:
            raise PermissionError(f'User {user.chat_id} evoked broadcast on {event.title}')
    else:
        if 'admin' in user.privileges:
            users = TlgUser.objects.all()
            user_count = broadcast(users, context.user_data['broadcast_text'], context)
            update.message.reply_text(
                strings['ru']['success_broadcast'].format(user_count=user_count),
                reply_markup=build_keyboard(update.effective_user.id)
            )
            return ConversationHandler.END
        else:
            raise PermissionError(f'User {user.chat_id} evoked common broadcast')


_r = r'^(?!\/cancel$).*'

broadcast_conversation = ConversationHandler(
    entry_points=[
        CallbackQueryHandler(creating_broadcast,
                             pattern=rf"^{indicators['broadcast']}::.*$"),
        MessageHandler(Filters.regex(f"^{strings['ru']['general_broadcast_button']}$"),
                       creating_broadcast),
    ],
    states={
        states['B_TEXT_CONFIRMING']: [
            MessageHandler(Filters.regex(_r), confirm_broadcast),
        ],
        states['B_DONE_ASKING']: [
            CommandHandler('accept', done_broadcast),
        ],
    },
    fallbacks=[CommandHandler('cancel', common_handlers.cancel_handler)],
)
