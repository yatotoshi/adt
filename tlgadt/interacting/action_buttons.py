import datetime
import logging
import re
from typing import Type

import pytz

from resources import *
from tlgadt.interacting.tools import *
from webadt.models import TlgUser, Event, Feedback

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class ActionButton:
    text: str
    _callback_arguments: str
    _context: CallbackContext
    _update: Update
    _user_id: int
    _paginator_type: str
    _callback_arguments: dict
    indicator: str

    def __init__(self, text: str, callback_arguments: dict,
                 update: Update, context: CallbackContext = None):
        self.text = text
        self._callback_arguments = callback_arguments
        self._context = context
        self._update = update
        self._user_id = update.effective_user.id
        self.indicator = self.get_indicator()

    @staticmethod
    def parse_arguments(callback_data: str) -> (str, dict[str]):
        def parse_cb_data(cb_data: str) -> dict[str]:
            arg_list = args.split('&')
            arg_dict = {}
            for arg in arg_list:
                key, value = arg.split('=')
                arg_dict[key] = value
            return arg_dict

        indicator_found = callback_data[0] == 'i'
        separator_found = callback_data.find('::') != -1
        arguments_found = callback_data.find('=') != -1
        if indicator_found and not separator_found and not arguments_found:
            return callback_data, {}
        elif not indicator_found and not separator_found and arguments_found:
            return None, parse_cb_data(callback_data)
        elif indicator_found and separator_found and arguments_found:
            indicator, args = callback_data.split('::')
            return indicator, parse_cb_data(callback_data)
        else:
            raise ValueError(f'Invalid callback data format {callback_data}')

    def get_callback_query(self):
        return '&'.join([f'{key}={self._callback_arguments[key]}' for key in self._callback_arguments])

    def render(self) -> InlineKeyboardButton:
        if self.exists():
            self.proceed()
            key = InlineKeyboardButton(
                text=self.text,
                callback_data=f'{self.get_indicator()}::{self.get_callback_query()}'
            )
            return key
        else:
            raise self.ButtonDoesNotExists(
                f'Attempt to render {self.__class__.__name__} button that does not exists '
                f'with callback_data: {self._callback_arguments} by user: {self._user_id}'
            )

    @property
    def callback_arguments(self):
        return self._callback_arguments

    def exists(self) -> bool:
        logger.debug(f'{self._user_id} evoked exist on {self}')
        return True

    def proceed(self) -> None:
        logger.debug(f'{self._user_id} evoked proceed {self}')

    def act(self):
        if not self.exists():
            raise self.ButtonDoesNotExists(
                f'Attempt to act on {self.__class__.__name__} button that does not exists '
                f'with callback_data: {self._callback_data} by user: {self._user_id}'
            )
        else:
            self.restrictor()
            logger.debug(f'{self._user_id} evoked act {self}')

    def get_effect(self) -> callable:
        return lambda button: button

    def restrictor(self) -> None:
        pass

    @staticmethod
    def listed() -> bool:
        return True

    @staticmethod
    def inheritor(indicator: str):
        for ButtonType in ActionButton.__subclasses__():
            if ButtonType.get_indicator() == indicator:
                return ButtonType

    @staticmethod
    def list_all() -> list:
        inheritors = [inh for inh in ActionButton.__subclasses__() if inh.listed()]
        return inheritors

    @staticmethod
    def get_indicator() -> str:
        return ''

    class ButtonDoesNotExists(Exception):
        pass

    def __str__(self) -> str:
        return f'[{self.__class__.__name__}] {self.text}'


class EventActionButton(ActionButton):
    def __init__(self, event: Event, paginator_type: str, page: int,
                 update: Update, context: CallbackContext = None):
        if event:
            self._event = event
        else:
            args = ActionButton.parse_arguments(update.callback_query.data)
            self._event = Event.objects.get(id=args['event_id'])

        super().__init__(
            text=self.text,
            callback_arguments={
                'event_id': self._event.id,
                'paginator': paginator_type,
                'page': page
            },
            update=update,
            context=context
        )
        self._user = TlgUser.objects.filter(chat_id=self._user_id).first()

    @staticmethod
    def inheritor(indicator: str):
        for ButtonType in EventActionButton.__subclasses__():
            if ButtonType.get_indicator() == indicator:
                return ButtonType


class SubscribeToggleButton(EventActionButton):
    subscribe_text = strings['ru']['subscribe_button']
    unsubscribe_text = strings['ru']['unsubscribe_button']
    text = '-'

    @staticmethod
    def get_indicator() -> str:
        return indicators['subscribe_toggle']

    def exists(self) -> bool:
        user_listener = self._user and 'listener' in self._user.privileges
        msk_tz = pytz.timezone('Europe/Moscow')
        event_current = self._event.deadline > get_today()
        free_seats = (self._event.listeners.all().count() < (self._event.seats or 9999) or
                      self._user in self._event.listeners.all())
        return user_listener and event_current and free_seats and super().exists()

    def proceed(self):
        super().proceed()
        if self._user.subscriptions.filter(id=self._event.id).exists():
            self.text = self.unsubscribe_text
        else:
            self.text = self.subscribe_text

    def act(self):
        super().act()
        if self._user.subscriptions.filter(id=self._event.id).exists():
            self._user.subscriptions.remove(self._event)
        else:
            self._user.subscriptions.add(self._event)

    def restrictor(self) -> None:
        if not (self._user and 'listener' in self._user.privileges):
            raise PermissionError('You need to have listener permissions')


class VisitedToggleButton(EventActionButton):
    set_visited_text = strings['ru']['set_visited_button']
    unset_visited_text = strings['ru']['unset_visited_button']
    text = '-'

    @staticmethod
    def get_indicator() -> str:
        return indicators['set_event_visited']

    def exists(self) -> bool:
        fb_exists = self._user.feedback_set.filter(event=self._event).exists()
        msk_tz = pytz.timezone('Europe/Moscow')
        event_passed = self._event.date < get_today()
        return fb_exists and event_passed and super().exists()

    def proceed(self):
        super().proceed()
        if self._user.feedback_set.get(event=self._event).visited:
            self.text = self.unset_visited_text
        else:
            self.text = self.set_visited_text

    def act(self):
        super().act()
        feedback = self._user.feedback_set.get(event=self._event)
        feedback.visited = not feedback.visited
        feedback.save()

    def restrictor(self) -> None:
        if not (self._user and 'listener' in self._user.privileges):
            raise PermissionError('You need to have listener permissions')


class EditEventButton(EventActionButton):
    text = strings['ru']['edit_event_button']

    @staticmethod
    def get_indicator() -> str:
        return indicators['edit_event']

    def exists(self) -> bool:
        user_owner = self._event.organizer == self._user or 'admin' in self._user.privileges
        return user_owner and super().exists()

    def restrictor(self) -> None:
        if not ('organizer' in self._user.privilegesnot):
            raise PermissionError('You need to have organizer permissions')


class DeleteEventButton(EventActionButton):
    text = strings['ru']['delete_event_button']

    @staticmethod
    def get_indicator() -> str:
        return indicators['delete_event']

    def exists(self) -> bool:
        user_owner = self._event.id and self._event.organizer == self._user or 'admin' in self._user.privileges
        return user_owner and super().exists()

    def get_effect(self) -> callable:
        def effect(button: InlineKeyboardButton):
            if type(button) == DeleteEventButton:
                button = ConfirmedDeleteEventButton(
                    event=self._event,
                    paginator_type=self.callback_arguments['paginator'],
                    page=self.callback_arguments['page'],
                    update=self._update,
                    context=self._context
                )
            return button
        return effect

    def restrictor(self) -> None:
        if not (self._user == self._event.organizer or 'admin' in self._user.privileges):
            raise PermissionError('You need to be the owner of the event')


class ConfirmedDeleteEventButton(EventActionButton):
    text = strings['ru']['confirmed_delete_event_button']

    @staticmethod
    def get_indicator() -> str:
        return indicators['confirmed_delete_event']

    def exists(self) -> bool:
        user_owner = self._event.id and self._event.organizer == self._user or 'admin' in self._user.privileges
        return user_owner and super().exists()

    def act(self):
        super().act()
        if self._user == self._event.organizer or 'admin' in self._user.privileges:
            self._event.delete()

    def restrictor(self) -> None:
        if not (self._user == self._event.organizer or 'admin' in self._user.privileges):
            raise PermissionError('You need to be the owner of the event')


class FeedbackButton(EventActionButton):
    text = '-'

    @staticmethod
    def get_indicator() -> str:
        return indicators['confirmed_delete_event']


class HeartToggleButton(EventActionButton):
    heart_text = strings['ru']['heart_event_button']
    unheart_text = strings['ru']['unheart_event_button']
    text = '-'

    @staticmethod
    def get_indicator() -> str:
        return indicators['heart_event']

    def exists(self) -> bool:
        fb = self._user.feedback_set.filter(event=self._event)
        return fb.exists() and fb[0].visited and super().exists()

    def proceed(self):
        super().proceed()
        fb = self._user.feedback_set.filter(event=self._event)
        if fb.exists() and fb[0].stars:
            self.text = self.unheart_text
        else:
            self.text = self.heart_text

    def act(self):
        super().act()
        fb = self._user.feedback_set.get(event=self._event)

        if fb.stars:
            fb.stars = None
        else:
            fb.stars = 1
        fb.save()

    def restrictor(self) -> None:
        if not (self._user and 'listener' in self._user.privileges):
            raise PermissionError('You need to have listener permissions')


class BroadcastButton(EventActionButton):
    text = strings['ru']['broadcast']

    @staticmethod
    def get_indicator() -> str:
        return indicators['broadcast']

    def exists(self) -> bool:
        user_owner = self._event.organizer == self._user or 'admin' in self._user.privileges
        return user_owner and super().exists()

    def restrictor(self) -> None:
        if not (self._user == self._event.organizer or 'admin' in self._user.privileges):
            raise PermissionError('You need to be the owner of the event')


class MoveActionButton(ActionButton):
    def __init__(self, direction: str, paginator_type: str, page: int,
                 update: Update, context: CallbackContext = None):
        text = ''
        if direction == 'fwd':
            text = '>'
        elif direction == 'bwd':
            text = '<'
        else:
            ValueError(f'Invalid direction: {direction}')
        super().__init__(
            text=text,
            callback_arguments={'direction': direction, 'page': page, 'paginator': paginator_type},
            update=update,
            context=context
        )

    @staticmethod
    def get_indicator() -> str:
        return indicators['page_events']

    def act(self):
        super().act()
        page_inc = 0
        if self._callback_arguments['direction'] == 'fwd':
            page_inc = +1
        elif self._callback_arguments['direction'] == 'bwd':
            page_inc = -1
        self._callback_arguments['page'] = int(self._callback_arguments['page']) + page_inc
