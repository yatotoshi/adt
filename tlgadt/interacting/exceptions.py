class UnknownIndicatorError(Exception):
    def __init__(self, indicator):
        super().__init__(f'Unknown indicator has been passed: {indicator}')


class InvalidDataError(Exception):
    pass