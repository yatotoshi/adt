import logging
from telegram import ReplyKeyboardMarkup, InlineKeyboardMarkup
from telegram.ext import CommandHandler, MessageHandler, Filters, ConversationHandler
from resources import *
import tlgadt.interacting.common_handlers as common_handlers
from .tools import *
import datetime

logger = logging.getLogger(__name__)


@privileges_required(['viewer'])
def change_name_handler(update: Update, _: CallbackContext) -> int:
    args = update.message.text.split()
    new_name = ' '.join(args[1:])
    if len(new_name) >= 256:
        response = strings['ru']['too_long_name_error']
    else:
        user = TlgUser.objects.get(chat_id=update.effective_user.id)
        user.name = new_name
        user.save()
        response = beautify(user, strings['ru']['success'])
    update.message.reply_text(
        text=response,
        reply_markup=build_keyboard(update.effective_user.id)
    )


@privileges_required(['viewer'])
def account_handler(update: Update, _: CallbackContext) -> int:
    args = update.message.text.split()
    user_filter = TlgUser.objects.filter(chat_id=update.effective_user.id)
    user = None
    if user_filter:
        user = user_filter[0]
        if 'admin' in user.privileges and len(args) > 1:
            another_user_filter = TlgUser.objects.filter(chat_id=args[1])
            if another_user_filter:
                another_user = another_user_filter[0]
                response = beautify(another_user)
            else:
                response = strings['ru']['no_user_found_error']
        else:
            response = beautify(user)
    else:
        response = strings['ru']['not_signup_error']
    update.message.reply_text(
        text=response,
        reply_markup=build_keyboard(update.effective_user.id)
    )


@viewer_only_required
def signup_handler(update: Update, _: CallbackContext) -> int:
    """Start signup process when the command /signup is issued."""
    logger.info(f'User {update.effective_user.id} started signup process with /signup command')
    update.message.reply_text(strings['ru']['signup_try'])
    update.message.reply_text(
        text=strings['ru']['signup_name_ask'],
        reply_markup=ReplyKeyboardMarkup(key_lists['cancel'], one_time_keyboard=True)
    )
    return states['NAME_ASKING']


@viewer_only_required
def accept_new_user(update: Update, _: CallbackContext) -> int:
    new_user = TlgUser(chat_id=update.effective_user.id, name=update.message.text)
    try:
        new_user.username = update.effective_user.username
    except:
        pass
    new_user.privileges = ['viewer']
    new_user.save()
    update.message.reply_text(
        strings['ru']['signup_success'],
        reply_markup=build_keyboard(update.effective_user.id)
    )
    update.message.reply_text(
        text=strings['ru']['signup_resume'].format(chat_id=new_user.chat_id, name=new_user.name),
        reply_markup=build_keyboard(update.effective_user.id)
    )
    return ConversationHandler.END


signup_conversation = ConversationHandler(
        entry_points=[
            CommandHandler('signup', signup_handler),
            MessageHandler(Filters.regex(f"^{strings['ru']['signup_button']}$"), signup_handler)
        ],
        states={
            states['NAME_ASKING']: [
                MessageHandler(Filters.regex(r'^(?!\/cancel$).*'), accept_new_user),
                CommandHandler('cancel', common_handlers.cancel_handler)
            ]
        },
        fallbacks=[CommandHandler('cancel', common_handlers.cancel_handler)],
    )
