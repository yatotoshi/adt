import logging
from telegram import ReplyKeyboardMarkup, InlineKeyboardMarkup
from telegram.ext import CommandHandler, MessageHandler, Filters, ConversationHandler
from resources import *
import tlgadt.interacting.common_handlers as common_handlers
from .tools import *
import datetime


@privileges_required(['admin'])
def set_organizer_handler(update: Update, _: CallbackContext) -> None:
    args = update.message.text.split()
    if len(args) != 2:
        update.message.reply_text(
            text=strings['ru']['set_organizer_error'],
            reply_markup=build_keyboard(update.effective_user.id)
        )
    else:
        user_to_set_filter = TlgUser.objects.filter(chat_id=args[1])
        if user_to_set_filter:
            user_to_set = user_to_set_filter[0]
            user_to_set.privileges += ['organizer']
            user_to_set.save()
            response = strings['ru']['success']
        else:
            response = strings['ru']['no_user_found_error']
    update.message.reply_text(
        text=response,
        reply_markup=build_keyboard(update.effective_user.id)
    )


@privileges_required(['admin'])
def unset_organizer_handler(update: Update, _: CallbackContext) -> None:
    args = update.message.text.split()
    if len(args) != 2:
        update.message.reply_text(
            text=strings['ru']['unset_organizer_error'],
            reply_markup=build_keyboard(update.effective_user.id)
        )
    else:
        user_to_unset_filter = TlgUser.objects.filter(chat_id=args[1])
        if user_to_unset_filter:
            user_to_unset = user_to_unset_filter[0]
            privileges = user_to_unset.privileges
            if 'organizer' in privileges:
                privileges.remove('organizer')
                user_to_unset.privileges = privileges
                user_to_unset.save()
            response = strings['ru']['success']
        else:
            response = strings['ru']['no_user_found_error']
    update.message.reply_text(
        text=response,
        reply_markup=build_keyboard(update.effective_user.id)
    )
