import logging

import pytz
from telegram.ext import CommandHandler, MessageHandler, Filters, CallbackQueryHandler
from telegram import ReplyKeyboardMarkup

from webadt.models import Event
from .action_buttons import ActionButton
from .tools import *
import tlgadt.interacting.common_handlers as common_handlers
import datetime
from .exceptions import InvalidDataError
from django.utils.dateparse import parse_datetime

logger = logging.getLogger(__name__)

event_states = [
        None,
        {'field': 'title', 'type': str, 'value': None, 'required': True},
        {'field': 'description', 'type': str, 'value': None, 'required': True},
        {'field': 'date', 'type': datetime.datetime, 'value': None, 'required': True},
        {'field': 'deadline', 'type': datetime.date, 'value': None, 'required': False},
        {'field': 'seats', 'type': int, 'value': None, 'required': False},
        {'field': 'lectors', 'type': str, 'value': None, 'required': False},
        {'field': 'done', 'required': False},
    ]


def creating_event(update: Update, context: CallbackContext) -> str:
    context.user_data['event_id'] = None
    context.user_data['states'] = event_states
    context.user_data['current_index'] = 0
    return ask_event_info_handler(update, context)


def ask_event_info_handler(update: Update, context: CallbackContext) -> str:
    event_states = context.user_data['states']
    current_index = context.user_data['current_index']

    if update.message and update.message.text == '/back':
        current_index = max(1, current_index - 1)
    elif event_states[current_index]:
        message_text = update.message.text_html
        if message_text == '/pass' and not event_states[current_index]['required']:
            current_index += 1
        elif message_text == '/pass' and event_states[current_index]['required']:
            update.message.reply_text(strings['ru']['event_state_unpassable_message'])
        elif event_states[current_index]['type'] == int:
            try:
                event_states[current_index]['value'] = int(message_text)
                current_index += 1
            except ValueError:
                update.message.reply_text(strings['ru']['invalid_int_message'])
        elif event_states[current_index]['type'] == datetime.datetime:
            try:
                event_states[current_index]['value'] = datetime.datetime.strptime(message_text, '%Y-%m-%d %H:%M')
                current_index += 1
            except ValueError:
                update.message.reply_text(strings['ru']['invalid_date_message'])
        elif event_states[current_index]['type'] == datetime.date:
            try:
                event_states[current_index]['value'] = datetime.datetime.strptime(message_text, '%Y-%m-%d')
                current_index += 1
            except ValueError:
                update.message.reply_text(strings['ru']['invalid_date_message'])
        elif event_states[current_index]['type'] == str:
            event_states[current_index]['value'] = message_text
            current_index += 1
        else:
            raise ValueError(f"Unknown field type passed: {event_states[current_index]['type']}")

    current_index = max(1, current_index)

    field_to_ask = event_states[current_index]['field']
    ask_message = strings['ru'][f"event_asking_{field_to_ask}_message"]
    buttons = []
    if not event_states[current_index]['required']:
        buttons += key_lists['pass']
    buttons += key_lists['back'] + key_lists['cancel']
    message = update.message or update.callback_query.message
    message.reply_text(
        text=ask_message,
        reply_markup=ReplyKeyboardMarkup(buttons)
    )
    context.user_data['current_index'] = current_index

    if event_states[current_index]['field'] != 'done':
        next_field = event_states[current_index + 1]['field']
        next_state = states[f"E_{next_field.upper()}_ASKING"]
        return next_state
    else:
        return done_creating_event(update, context)


@privileges_required(['organizer'])
def done_creating_event(update: Update, context: CallbackContext) -> str:
    event_id = context.user_data['event_id']
    event_info = {}
    for state in context.user_data['states'][1:-1]:
        event_info[state['field']] = state['value']
        if state['value'] is None and state['type'] == str:
            event_info[state['field']] = ''
    user = TlgUser.objects.get(chat_id=update.effective_user.id)

    if not event_id:
        event = Event(
            title=event_info['title'],
            description=event_info['description'],
            date=event_info['date'],
            organizer=user,
            deadline=event_info['deadline'],
            lectors=event_info['lectors'],
            seats=event_info['seats'],
        )
    else:
        event = Event.objects.get(id=int(event_id))
        event.title = event_info['title']
        event.description = event_info['description']
        event.date = event_info['date']
        event.deadline = event_info['deadline']
        event.lectors = event_info['lectors']
        event.eats = event_info['seats']
    try:
        event.save()
        event = Event.objects.get(id=event.id)
        reply = beautify(event)
    except Exception as e:
        reply = strings['ru']['error_occurred']
        logger.error(e)
    finally:
        context.user_data.clear()
        update.message.reply_text(
            text=reply,
            reply_markup=build_keyboard(update.effective_user.id)
        )
        return ConversationHandler.END


@privileges_required(['organizer'])
def edit_event_handler(update: Update, context: CallbackContext) -> str:
    indicator, args = ActionButton.parse_arguments(update.callback_query.data)
    event = Event.objects.get(id=int(args['event_id']))
    user = TlgUser.objects.get(chat_id=update.effective_user.id)
    if event.organizer != user:
        return ConversationHandler.END

    context.user_data['event_id'] = event.id
    context.user_data['states'] = event_states
    context.user_data['current_index'] = 0
    return ask_event_info_handler(update, context)


_r = r'^(?!\/cancel$).*'

create_event_conversation = ConversationHandler(
    entry_points=[
        MessageHandler(Filters.regex(f"^{strings['ru']['create_event_button']}$"), creating_event),
        CallbackQueryHandler(edit_event_handler,
                             pattern=rf"^{indicators['edit_event']}::.*$")
    ],
    states={
        states['E_DESCRIPTION_ASKING']: [
            MessageHandler(Filters.regex(_r), ask_event_info_handler),
        ],
        states['E_DATE_ASKING']: [
            MessageHandler(Filters.regex(_r), ask_event_info_handler),
        ],
        states['E_DEADLINE_ASKING']: [
            MessageHandler(Filters.regex(_r), ask_event_info_handler),
        ],
        states['E_LECTORS_ASKING']: [
            MessageHandler(Filters.regex(_r), ask_event_info_handler),
        ],
        states['E_SEATS_ASKING']: [
            MessageHandler(Filters.regex(_r), ask_event_info_handler),
        ],
        states['E_DONE_ASKING']: [
            MessageHandler(Filters.regex(_r), ask_event_info_handler),
        ],
    },
    fallbacks=[CommandHandler('cancel', common_handlers.cancel_handler)],
)
