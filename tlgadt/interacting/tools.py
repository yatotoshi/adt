import functools
import datetime
from typing import Type, Callable

import pytz
from django.db.models import QuerySet
from telegram import Update, ReplyKeyboardMarkup, InlineKeyboardMarkup, Bot
from telegram.ext import CallbackContext, ConversationHandler
from webadt.models import TlgUser
from resources import *
from django.db import models


def privileges_required(privileges: list[str]):
    def decorator(handler):
        @functools.wraps(handler)
        def wrapper_decorator(update: Update, context: CallbackContext):
            if TlgUser.objects.filter(chat_id=update.effective_user.id).exists():
                user = TlgUser.objects.get(chat_id=update.effective_user.id)
                user_privileges = user.get_privileges()
            else:
                user_privileges = ['viewer']

            for status in privileges:
                if status not in user_privileges:
                    update.message.reply_text(
                        text=strings['ru']['no_privileges'],
                        reply_markup=build_keyboard(update.effective_user.id)
                    )
                    return ConversationHandler.END
            return handler(update, context)
        return wrapper_decorator
    return decorator


def viewer_only_required(handler):
    @functools.wraps(handler)
    def wrapper_decorator(update: Update, context: CallbackContext):
        if (not TlgUser.objects.filter(chat_id=update.effective_user.id).exists()) \
                or TlgUser.objects.get(chat_id=update.effective_user.id).get_privileges() == ['viewer']:
            return handler(update, context)
        else:
            update.message.reply_text(strings['ru']['signup_user_exists'])
    return wrapper_decorator


def build_keyboard(chat_id: int) -> ReplyKeyboardMarkup:
    keyboard = []
    try:
        privileges = TlgUser.objects.get(chat_id=chat_id).privileges
    except TlgUser.DoesNotExist:
        privileges = ['viewer']
    if privileges == ['viewer']:
        keyboard += key_lists['signup']
    for status in privileges:
        keyboard += key_lists[status]
    return ReplyKeyboardMarkup(keyboard, one_time_keyboard=False)


def beautify(obj, title=None) -> str:
    if title:
        if obj.id:
            return title + '\n\n' + obj.__beautify__('ru')
        else:
            return strings['ru']['deleted']
    else:
        return obj.__beautify__('ru')


def get_today():
    return datetime.datetime.now(tz=pytz.utc) + datetime.timedelta(hours=3)


def broadcast(users: QuerySet, message: str, context: CallbackContext) -> int:
    user_count = 0
    for user in users:
        try:
            context.bot.sendMessage(
                chat_id=user.chat_id,
                text=message
            )
            user_count += 1
        except:
            pass
    return user_count
