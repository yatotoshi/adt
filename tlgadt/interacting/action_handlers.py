from telegram import Update
from telegram.ext import CallbackContext

from tlgadt.interacting.action_buttons import ActionButton, EventActionButton, MoveActionButton
from tlgadt.interacting.paginators import EventPaginator, FindEventPaginator
from tlgadt.interacting.tools import beautify
from webadt.models import Event
from resources import *


def action_handler(update: Update, context: CallbackContext) -> None:
    indicator, callback_arguments = ActionButton.parse_arguments(update.callback_query.data)
    event = Event.objects.get(id=callback_arguments['event_id'])
    button = EventActionButton.inheritor(indicator)(
        event=event,
        paginator_type=callback_arguments['paginator'],
        page=callback_arguments['page'],
        update=update,
        context=context
    )
    button.proceed()
    button.act()
    button_effect = button.get_effect()
    if button.callback_arguments['paginator'] == 'find':
        paginator = FindEventPaginator(
            event_short=event.short,
            update=update,
            context=context,
            button_effect=button_effect
        )
    else:
        paginator = EventPaginator(
            paginator_type=button.callback_arguments['paginator'],
            current_page=button.callback_arguments['page'],
            button_effect=button_effect,
            update=update,
            context=context
        )
    event, keyboard = paginator.get_page()
    if event:
        update.callback_query.message.edit_text(
            text=beautify(event, paginator.get_title()),
            reply_markup=keyboard
        )
    else:
        update.callback_query.message.edit_text(
            text=strings['ru']['nothing_to_show'],
            reply_markup=keyboard
        )


def move_handler(update: Update, context: CallbackContext) -> None:
    indicator, callback_arguments = ActionButton.parse_arguments(update.callback_query.data)
    button = MoveActionButton(
        paginator_type=callback_arguments['paginator'],
        page=callback_arguments['page'],
        direction=callback_arguments['direction'],
        update=update,
        context=context
    )
    button.proceed()
    button.act()

    paginator = EventPaginator(
        paginator_type=button.callback_arguments['paginator'],
        current_page=button.callback_arguments['page'],
        update=update,
        context=context
    )
    event, keyboard = paginator.get_page()
    if event:
        update.callback_query.message.edit_text(
            text=beautify(event, paginator.get_title()),
            reply_markup=keyboard
        )
    else:
        update.callback_query.message.edit_text(
            text=strings['ru']['nothing_to_show'],
            reply_markup=keyboard
        )
