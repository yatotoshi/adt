import datetime
from typing import Type, Callable

import pytz
from django.db import models
from django.db.models import QuerySet
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update
from telegram.ext import CallbackContext


import tlgadt.interacting.action_buttons as action_buttons
from resources import strings
from tlgadt.interacting.tools import get_today
from webadt.models import TlgUser, Event


class EventPaginator:
    _page: int
    _query_set: models.query.QuerySet
    _paginator_type: str
    _action_buttons_types: list[Type[action_buttons.EventActionButton]]
    _update: Update
    _context: CallbackContext
    _button_effect: callable
    _message_effect: callable
    _title: str

    def __init__(self, paginator_type: str, current_page,
                 update: Update, context: CallbackContext,
                 button_effect: callable = None, message_effect: callable = None):
        self._page = int(current_page)
        self._paginator_type = paginator_type
        if button_effect:
            self._button_effect = button_effect
        else:
            self._button_effect = lambda button: button
        if message_effect:
            self._message_effect = message_effect
        else:
            self._message_effect = lambda message_text: message_text
        self._message_effect = message_effect
        self._update = update
        self._context = context
        self._query_set, self._action_buttons_types, self._title = self.get_params()

    def get_title(self):
        return self._title

    def forward_available(self) -> bool:
        return self._page < self._query_set.count() - 1

    def backward_available(self) -> bool:
        return self._page > 0

    def get_params(self) -> (QuerySet, list):
        t = self._paginator_type
        if t == 'current':
            qs = Event.objects.filter(date__gte=str(get_today())).order_by('date')
            action_buttons_types = [
                action_buttons.SubscribeToggleButton
            ]
        elif t == 'subscriptions':
            user = TlgUser.objects.get(chat_id=self._update.effective_user.id)
            qs = user.subscriptions.filter(date__gt=str(get_today())).order_by('date')
            action_buttons_types = [
                action_buttons.SubscribeToggleButton,
            ]
        elif t == 'visited':
            user = TlgUser.objects.get(chat_id=self._update.effective_user.id)
            qs = user.subscriptions.filter(date__lte=str(get_today())).order_by('-date')
            action_buttons_types = [
                action_buttons.VisitedToggleButton,
                action_buttons.HeartToggleButton,
            ]
        elif t == 'created':
            user = TlgUser.objects.get(chat_id=self._update.effective_user.id)
            qs = Event.objects.filter(date__gte=str(get_today()),
                                      organizer=user).order_by('date')
            action_buttons_types = [
                action_buttons.SubscribeToggleButton,
                action_buttons.BroadcastButton,
                action_buttons.EditEventButton,
                action_buttons.DeleteEventButton,
            ]
        elif t == 'archive':
            user = TlgUser.objects.get(chat_id=self._update.effective_user.id)
            qs = Event.objects.filter(date__lt=str(get_today()),
                                      organizer=user).order_by('-date')
            action_buttons_types = [
                action_buttons.VisitedToggleButton,
                action_buttons.HeartToggleButton,
                action_buttons.DeleteEventButton,
            ]
        else:
            raise ValueError(f'Unknown paginator type: {t}')
        return qs, action_buttons_types, strings['ru'][f'{t}_events_message_title']

    def get_page(self) -> (models.Model, InlineKeyboardMarkup):
        count = self._query_set.count()
        if count == 0:
            return None, None
        max_page = count - 1
        self._page = page = min(max(0, self._page), max_page)
        event = self._query_set[page]

        move_row = []
        if self.backward_available():
            bwd_action_button = action_buttons.MoveActionButton(
                direction='bwd',
                paginator_type=self._paginator_type,
                page=self._page,
                update=self._update,
                context=self._context
            )
            move_row.append(bwd_action_button.render())
        if self.forward_available():
            fwd_action_button = action_buttons.MoveActionButton(
                direction='fwd',
                paginator_type=self._paginator_type,
                page=self._page,
                update=self._update,
                context=self._context
            )
            move_row.append(fwd_action_button.render())

        button_column = self.render_action_buttons(event)
        button_column.append(move_row)
        return event, InlineKeyboardMarkup(button_column)

    def move(self, direction):
        if direction == 'fwd':
            self._page += 1
        elif direction == 'bwd':
            self._page -= 1
        else:
            ValueError(f'Invalid direction: {direction}')
        return self.get_page()

    def render_action_buttons(self, event=None):
        action_buttons = []
        for ButtonType in self._action_buttons_types:
            button = ButtonType(
                update=self._update,
                paginator_type=self._paginator_type,
                page=self._page,
                context=self._context,
                event=event
            )
            button = self._button_effect(button)
            if not button.exists():
                continue
            button.proceed()
            action_buttons.append([button.render()])
        return action_buttons


class FindEventPaginator(EventPaginator):
    event_short: str

    def __init__(self, event_short: str, update: Update, context: CallbackContext,
                 button_effect: callable = None, message_effect: callable = None):
        self.event_short = event_short
        super().__init__(
            paginator_type='find',
            current_page=0,
            update=update,
            context=context,
            button_effect=button_effect,
            message_effect=message_effect
        )

    def move(self, direction):
        return self.get_page()

    def get_params(self):
        action_button_types = [
            action_buttons.SubscribeToggleButton,
            action_buttons.VisitedToggleButton,
            action_buttons.HeartToggleButton,
            action_buttons.EditEventButton,
            action_buttons.DeleteEventButton,
            action_buttons.BroadcastButton
        ]
        return Event.objects.filter(short=self.event_short), action_button_types, None
