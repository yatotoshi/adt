import os
import logging
from pathlib import Path
from .connection import connect
from .interacting import handler_list


def run() -> None:
    logging.basicConfig(
        filename='debug.log',
        format='%(asctime)s.%(msecs)03d - %(module)s [%(levelname)s] %(message)s', datefmt='%d.%m.%y %H:%M:%S',
        level=logging.INFO,
    )
    logging.getLogger().addHandler(logging.StreamHandler())

    logger = logging.getLogger(__name__)
    logger.info('Running')

    connect(handler_list)


if __name__ == '__main__':
    run()
